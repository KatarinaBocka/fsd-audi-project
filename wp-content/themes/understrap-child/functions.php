<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

/**
 * Enqueue a script
 */
add_action( 'wp_enqueue_scripts', 'understrap_load_scripts' );
function understrap_load_scripts(){
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/node_modules/jquery/dist/jquery.min.js', array(), null, true );
    // Main script 
     wp_enqueue_script( 'child-understrap-scripts', get_template_directory_uri() . '/js/custom-javascript.js', array(), '1.0.0', true );
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

/*
====================================
Register Menu Locations
Footer manu
====================================
*/
register_nav_menus( [
	'footer-menu' => esc_html__( 'Footer Menu', 'naturaland' ),
]);

/*
====================================
Add New Category
====================================
*/
function tr_create_my_taxonomy() {
    
    register_taxonomy(
    'product-category',
    'product',
        array(
        'label' => __( 'Product Category' ),
        'rewrite' => array( 'slug' => 'product-category' ),
        'hierarchical' => true,
        )
    );
}
add_action( 'init', 'tr_create_my_taxonomy' );

/*
====================================
Custom Post Type
====================================
*/
function create_custom_post_type () {
    $labels = array(
        'name' => 'Products',
        'singular_name' => 'Product',
        'add_new' => 'Add new Product',
        'all_items' => 'All Items',
        'add_new_item' => 'Add Item',
        'edit_item' => 'Edit Item',
        'new_item'=> 'New Item',
        'view_item' => 'View Item',
        'search_item' => 'Search Product',
        'not_found' => 'No items found',
        'not_found_in_trash' => 'No item found in trash',
        'parent_item_colon' => 'Parent Item'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array(
            'title',
            'thumbnail',
        ),
        'taxonomies' => array( 'product-category'),
        'menu_position' => 5,
        'exclude_from_search' => false,
        'menu_icon' => 'dashicons-cart',
        'rewrite' => array( 'slug' => 'products' )
    );
    register_post_type('products', $args);
}
add_action('init', 'create_custom_post_type');