<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>



<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

<?php get_template_part('components/contactSection'); ?>

	<div class="<?php echo esc_attr( $container ); ?>">

		<footer class="site-footer" id="colophon">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<!-- copyright text -->
						<div class="copy-wrapper">
							<h5 class="copyright-text">© <?php echo date("Y"); ?> FSD. All rights reserved.</h5>
						</div>
					</div>


				</div><!-- .row end -->
			</div><!-- .container end -->
		
						<!-- footer navigation -->
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<?php
								$args = ['theme_location' => 'footer-menu'];
								wp_nav_menu( $args );
							?>
						</nav>
				
		</footer>

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
