<?php 
$contactImage = get_field('contact_image');
echo get_field('contact_image'); 
?>
<div id="contact-section">
<img src="<?php echo $contactImage['url'] ?>" alt="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 contact-text">
            <?php echo get_field('contact_text'); ?>
          
            </div>
            <div class="col-sm-12 col-md-6 justify-content-center d-flex">
                <div class="form-section">
                <?php echo do_shortcode('[contact-form-7 id="106" title="Contact Us"]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>