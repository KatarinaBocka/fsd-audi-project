<div id="about-section">
  <div class="container">
    <div class="row">
      <div class="col-sm img-side">
        <div class="image-wrapper">
          <?php 
          $image = get_field('about_image');
          if( !empty( $image ) ): ?>
          <img src="<?php echo esc_url($image['url']); ?>" alt="">
          <?php endif; ?>
          <h5>A progressive design, a powerful engine.</h5>
        </div>
      </div>
      <div class="col-sm justify-content-center d-flex centet-block align-items-center">
          <div class="content-text">
            <?php echo get_field('about_content_text') ?>
          </div>
      </div>
    </div>
  </div>
</div>