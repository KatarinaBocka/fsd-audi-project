<?php 
    $background = get_field('featured_background');
    $content_text = get_field('featured_content_text');
    $blocks = get_field('featured_block');
    $i = 0;
    if( !empty( $background ) ): ?>

        <div id="featured-section" style="background: -webkit-gradient(linear, left top, left bottom, from(rgba(0, 0, 0, 0.1)), to(rgba(0, 0, 0, .6))), url(<?php echo esc_url($background['url']); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-7">
                        <?php echo $content_text ?>

                        <?php
                            foreach ($blocks as $block) {
                        ?>
                        <div class="block">
                            <?php echo $block['featured_block_content'] ?>
                        </div>
                        <?php $i++;} ?>
                    </div>

                    
                </div>
            </div>
        </div>

    <?php endif; ?>
