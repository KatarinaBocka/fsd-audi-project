

<div id="product-section" class="container">
  <h2>
    <?php echo get_field('product_section_header'); ?>
  </h2>
  <div class="row">

  <?php
    $args = array( 'post_type' => 'products', 'posts_per_page' => 4 );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
  ?>

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
      <div class="card">
        <img class="card-img-top" src="<?php echo the_post_thumbnail_url(); ?>" alt="Card image" style="width:100%">
        <div class="card-body">
          <h5 class="card-title"><?php echo get_field('property'); ?></h5>
          <h4 class="card-title"><?php echo get_the_title(); ?></h4>
          <ul class="offers">
            <?php
            $offers = get_field('sale_offers');
            $i = 0;
            //$sliderLength = count($slider);
            foreach ($offers as $offer) {
            ?>
            <li class="property"><img src="http://localhost:8000/wp-content/uploads/2020/08/Path@2x.png" alt=""><?php echo $offer['property']; ?></li>
            <?php $i++;} ?>
          </ul>
          <ul class="price">
            <li>Starting from: <span><?php echo get_field('price'); ?> &euro;</span></li>
            <li><a href="#" class="btn btn-primary">Buy Now</a></li>
          </ul>
          
        </div>
      </div><!-- .card end -->
    </div><!-- .col end -->


<?php endwhile;
wp_reset_query(); ?>

</div><!-- .row end -->
</div> <!-- .container end -->


