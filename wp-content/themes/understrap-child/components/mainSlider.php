<div id="mainCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <?php
        $slider = get_field('main_slider');
        $i = 0;
        //$sliderLength = count($slider);
        foreach ($slider as $slide) {
    ?>

    <div class="carousel-item <?php echo ($i == 0)?'active':'' ?>">
      <img class="d-block w-100" src="<?php echo $slide['image']['url']; ?>">
      <div class="carousel-caption">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12 wrapp-carousel-text">
              <?php echo $slide['title']; ?>
              <div class="vertical-line">
                <?php echo $slide['subtitle']; ?>
              </div>
							
						</div>
					</div>
				</div>
	    </div>   
    </div>

    <?php $i++;} ?>

  </div>
  <a class="carousel-control-prev carousel-control" href="#mainCarousel" role="button" data-slide="prev">
    <i class="fa fa-angle-left"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next carousel-control" href="#mainCarousel" role="button" data-slide="next">
  <i class="fa fa-angle-right"></i>
    <span class="sr-only">Next</span>
  </a>
</div>