<?php /* 
This page is used to display the static frontpage. 
*/
 
// Fetch theme header template
get_header(); ?>

<?php get_template_part('components/mainSlider'); ?>
<?php get_template_part('components/aboutSection'); ?>
<?php get_template_part('components/featuredSection'); ?>
<?php get_template_part('components/productSection'); ?>

<?php get_footer(); ?>